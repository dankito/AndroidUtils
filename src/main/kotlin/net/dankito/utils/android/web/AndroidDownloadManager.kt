package net.dankito.utils.android.web

import android.Manifest
import android.app.Activity
import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.database.Cursor
import android.net.Uri
import android.support.v7.app.AlertDialog
import net.dankito.utils.android.R
import net.dankito.utils.android.permissions.IPermissionsService
import org.slf4j.LoggerFactory
import java.io.File
import java.util.*
import java.util.concurrent.ConcurrentHashMap


open class AndroidDownloadManager(protected var context: Activity, protected var permissionsService: IPermissionsService? = null)
    : BroadcastReceiver(), IDownloadManager {

    companion object {
        protected val RESERVED_CHARACTERS = arrayOf('|', '\\', '?', '*', '<', '\'', '"', ':', '>', '[', ']', '/')

        private val log = LoggerFactory.getLogger(AndroidDownloadManager::class.java)
    }


    // TODO: save currentDownloaded so that on an App restart aborted and on restart finished downloads can be handled
    protected var currentDownloads: MutableMap<Long, CurrentDownload> = ConcurrentHashMap()


    protected val downloadManager: DownloadManager
        get() = context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager


    init {
        registerBroadcastReceivers(context)
    }

    protected open fun registerBroadcastReceivers(context: Activity) {
        context.registerReceiver(this, IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE))

        context.registerReceiver(this, IntentFilter(DownloadManager.ACTION_NOTIFICATION_CLICKED))
    }


    override fun downloadUrlAsync(downloadInfo: DownloadInfo, callback: (DownloadResult) -> Unit) {
        permissionsService?.let { permissionsService ->
            permissionsService.checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, "") { _, isPermitted ->
                if (isPermitted) {
                    downloadUrlAsyncWithPermissionsPermitted(downloadInfo, callback)
                }
            }
        } ?: run { // then we hope the calling application has asked user for permission to write external storage or writes to internal storage
            downloadUrlAsyncWithPermissionsPermitted(downloadInfo, callback)
        }
    }

    protected open fun downloadUrlAsyncWithPermissionsPermitted(downloadInfo: DownloadInfo, callback: (DownloadResult) -> Unit) {
        try {
            val uri = Uri.parse(downloadInfo.url)
            val request = DownloadManager.Request(uri)

            if (downloadInfo.downloadAllowedOverRoaming) {
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI.and(DownloadManager.Request.NETWORK_MOBILE))
                request.setAllowedOverRoaming(true)
            } else {
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI)
                request.setAllowedOverRoaming(false)
            }

            val filename = downloadInfo.destinationPath.name
            request.setTitle(filename)
            request.setDescription(filename)

            val safeFileName = removeReservedCharacters(filename)
            request.setDestinationUri(Uri.fromFile(File(downloadInfo.destinationPath.parent, safeFileName)))

            val currentDownload = CurrentDownload(downloadInfo, callback)
            currentDownloads[downloadManager.enqueue(request)] = currentDownload
        } catch (e: Exception) {
            log.error("Could not start Download for " + downloadInfo.url, e)
        }
    }

    protected open fun removeReservedCharacters(filename: String): String {
        var adjustedFilename = filename

        for (reservedCharacter in RESERVED_CHARACTERS) {
            adjustedFilename = adjustedFilename.replace(reservedCharacter, '_')
        }

        return adjustedFilename
    }


    override fun onReceive(context: Context, intent: Intent) {
        val action = intent.action

        when (action) {
            DownloadManager.ACTION_DOWNLOAD_COMPLETE -> handleDownloadCompleteBroadcast(intent)
            DownloadManager.ACTION_NOTIFICATION_CLICKED -> handleNotificationClickedBroadcast(intent)
        }
    }

    protected open fun handleDownloadCompleteBroadcast(intent: Intent) {
        val downloadId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1)

        if (currentDownloads.containsKey(downloadId)) { // yeah, we started this download
            currentDownloads.remove(downloadId)?.let { currentDownload ->
                try {
                    getEnqueuedDownloadForId(downloadId)?.let { enqueuedDownload ->
                        removeEnqueuedDownload(currentDownload, enqueuedDownload)
                    }
                } catch (e: Exception) {
                    log.error("Could not handle successful Download", e)
                }
            }
        }
    }

    protected open fun removeEnqueuedDownload(currentDownload: CurrentDownload, enqueuedDownload: EnqueuedDownload) {
        val downloadInfo = currentDownload.downloadInfo
        val callback = currentDownload.callback

        if (enqueuedDownload.wasSuccessful) {
            downloadInfo.downloadLocationUri = enqueuedDownload.downloadLocationUri

            callback(DownloadResult(true, downloadInfo))
        } else {
            callback(DownloadResult(downloadInfo, enqueuedDownload.reason))
        }
    }

    protected open fun getEnqueuedDownloadForId(downloadId: Long): EnqueuedDownload? {
        val query = DownloadManager.Query()
        query.setFilterById(downloadId)
        val cursor = downloadManager.query(query)

        if (cursor != null && cursor.moveToFirst()) {
            val result = deserializeDatabaseEntry(cursor)

            cursor.close()

            return result
        }

        return null
    }

    protected open fun deserializeDatabaseEntry(cursor: Cursor): EnqueuedDownload {
        return EnqueuedDownload(
                cursor.getLong(cursor.getColumnIndex(DownloadManager.COLUMN_ID)),
                cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_URI)),
                cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI)),
                cursor.getLong(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES)),
                cursor.getLong(cursor.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR)),
                cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS)),
                cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_REASON)),
                cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_TITLE)),
                cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_DESCRIPTION)),
                Date(cursor.getLong(cursor.getColumnIndex(DownloadManager.COLUMN_LAST_MODIFIED_TIMESTAMP))),
                cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_MEDIA_TYPE)),
                cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_MEDIAPROVIDER_URI))
        )
    }


    protected open fun handleNotificationClickedBroadcast(intent: Intent) {
        val downloadIds = intent.getLongArrayExtra(DownloadManager.EXTRA_NOTIFICATION_CLICK_DOWNLOAD_IDS)

        for (downloadId in downloadIds) {
            askShouldDownloadGetCancelled(downloadId)
        }
    }

    protected open fun askShouldDownloadGetCancelled(downloadId: Long) {
        val currentDownload = currentDownloads[downloadId]
        if (currentDownload != null) {
            askShouldDownloadGetCancelled(downloadId, currentDownload)
        } else { // on old download from previous run of this app
            removeDownloadFromDownloadManager(downloadId)
        }
    }

    protected open fun askShouldDownloadGetCancelled(downloadId: Long, currentDownload: CurrentDownload) {
        val downloadInfo = currentDownload.downloadInfo
        val filename = downloadInfo.destinationPath.name

        var builder = AlertDialog.Builder(context)
        builder = builder.setMessage(context.getString(R.string.alert_message_cancel_download, filename))

        builder.setNegativeButton(android.R.string.no, null)
        builder.setPositiveButton(android.R.string.yes) { _, _ -> cancelDownload(downloadId, downloadInfo) }

        builder.create().show()
    }

    protected open fun cancelDownload(downloadId: Long, downloadInfo: DownloadInfo) {
        removeDownloadFromDownloadManager(downloadId)

        val currentDownload = currentDownloads.remove(downloadId)

        if (currentDownload != null) {
            val callback = currentDownload.callback

            callback(DownloadResult(false, downloadInfo, true))
        }
    }

    protected open fun removeDownloadFromDownloadManager(downloadId: Long) {
        downloadManager.remove(downloadId)
    }

}
