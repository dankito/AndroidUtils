package net.dankito.utils.android.web

import java.io.File


class DownloadInfo(val url: String, val destinationPath: File, val downloadAllowedOverRoaming: Boolean = false,
                   var fileSize: String = "0", var downloadLocationUri: String? = null) {


    override fun toString(): String {
        return url
    }

}
