package net.dankito.utils.android.web


interface IDownloadManager {

    fun downloadUrlAsync(downloadInfo: DownloadInfo, callback: (DownloadResult) -> Unit)

}
