package net.dankito.utils.android.web


class DownloadResult(val successful: Boolean, val error: String?, val downloadInfo: DownloadInfo, val isUserCancelled: Boolean = false) {


    @JvmOverloads constructor(successful: Boolean, downloadInfo: DownloadInfo, isUserCancelled: Boolean = false)
        : this(successful, null, downloadInfo, isUserCancelled)

    constructor(downloadInfo: DownloadInfo, error: String) : this(false, error, downloadInfo)

}
