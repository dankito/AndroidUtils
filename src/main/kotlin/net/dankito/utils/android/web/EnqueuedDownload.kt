package net.dankito.utils.android.web

import java.util.*


class EnqueuedDownload(val id: Long,
                       val uri: String,
                       val downloadLocationUri: String,
                       val fileSize: Long,
                       val bytesDownloadedSoFar: Long = 0,
                       val status: String,
                       val reason: String,
                       val title: String,
                       val description: String,
                       val lastModified: Date,
                       val mediaType: String,
                       val mediaProviderUri: String?) {

    val isStatusOk: Boolean
        get() = "200" == status

    val wasSuccessful: Boolean
        get() = fileSize == bytesDownloadedSoFar


    override fun toString(): String {
        return uri
    }

}
