package net.dankito.utils.android.image

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.support.media.ExifInterface
import java.io.File
import java.io.IOException


open class AndroidImageUtils {

    @Throws(IOException::class)
    fun getImageOrientationInDegree(absoluteImagePath: String): Int {
        when (getImageOrientation(absoluteImagePath)) {
            ImageOrientation.RotatedBy90Deg -> return 90
            ImageOrientation.RotatedBy180Deg -> return 180
            ImageOrientation.RotatedBy270Deg -> return 270
            else -> return 0
        }
    }

    @Throws(IOException::class)
    fun getImageOrientation(absoluteImagePath: String): ImageOrientation {
        val exifInterface = ExifInterface(absoluteImagePath)
        val exifOrientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)

        return mapExifOrientation(exifOrientation)
    }

    protected open fun mapExifOrientation(exifOrientation: Int): ImageOrientation {
        when (exifOrientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> return ImageOrientation.RotatedBy90Deg

            ExifInterface.ORIENTATION_ROTATE_180 -> return ImageOrientation.RotatedBy180Deg

            ExifInterface.ORIENTATION_ROTATE_270 -> return ImageOrientation.RotatedBy270Deg

            ExifInterface.ORIENTATION_FLIP_HORIZONTAL -> return ImageOrientation.FlippedHorizontally

            ExifInterface.ORIENTATION_FLIP_VERTICAL -> return ImageOrientation.FlippedVertically

            else -> return ImageOrientation.Normal
        }
    }

    @Throws(IOException::class)
    fun getCorrectlyRotatedBitmap(imagePath: File): Bitmap? {
        return getCorrectlyRotatedBitmap(imagePath.absolutePath)
    }

    @Throws(IOException::class)
    fun getCorrectlyRotatedBitmap(absoluteImagePath: String): Bitmap? {
        val bitmap = BitmapFactory.decodeFile(absoluteImagePath)

        if (bitmap != null) {
            return correctOrientationIfNeeded(bitmap, absoluteImagePath)
        }

        return null
    }

    /**
     * Thank to user1922137 for providing this code (see https://stackoverflow.com/a/31927359)
     */
    @Throws(IOException::class)
    fun correctOrientationIfNeeded(bitmap: Bitmap, absoluteImagePath: String): Bitmap {
        val orientation = getImageOrientation(absoluteImagePath)

        when (orientation) {
            ImageOrientation.RotatedBy90Deg -> return rotate(bitmap, 90f)

            ImageOrientation.RotatedBy180Deg -> return rotate(bitmap, 180f)

            ImageOrientation.RotatedBy270Deg -> return rotate(bitmap, 270f)

            ImageOrientation.FlippedHorizontally -> return flip(bitmap, true, false)

            ImageOrientation.FlippedVertically -> return flip(bitmap, false, true)

            else -> return bitmap
        }
    }

    fun rotate(bitmap: Bitmap, degrees: Float): Bitmap {
        val matrix = Matrix()

        matrix.postRotate(degrees)

        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
    }

    fun flip(bitmap: Bitmap, horizontal: Boolean, vertical: Boolean): Bitmap {
        val matrix = Matrix()

        val sx = if (horizontal) -1f else 1f
        val sy = if (vertical) -1f else 1f

        matrix.preScale(sx, sy)

        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
    }

}