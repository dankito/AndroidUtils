package net.dankito.utils.android.image


enum class ImageOrientation {

    Normal,
    RotatedBy90Deg,
    RotatedBy180Deg,
    RotatedBy270Deg,
    FlippedHorizontally,
    FlippedVertically

}