package net.dankito.utils.android.keyboard


interface SoftKeyboardToggleListener {

    fun onToggleSoftKeyboard(isVisible: Boolean)

}