package net.dankito.utils.android.keyboard

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import java.util.*


/**
 * Thanks to ravindu1024 for providing the original version of this class:
 * https://github.com/ravindu1024/android-keyboardlistener/blob/master/keyboard-listener/src/main/java/com/rw/keyboardlistener/KeyboardUtils.java
 *
 * Based on the following Stackoverflow answer:
 * http://stackoverflow.com/questions/2150078/how-to-check-visibility-of-software-keyboard-in-android
 */
class KeyboardUtils {

    companion object {

        /**
         * Manually toggle soft keyboard visibility
         * @param context calling context
         */
        fun toggleKeyboardVisibility(context: Context) {
            val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
        }

        /**
         * Force closes the soft keyboard
         * @param activeView the view with the keyboard focus
         */
        fun forceCloseKeyboard(activeView: View) {
            val inputMethodManager = activeView.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(activeView.windowToken, 0)
        }
    }


    private val listeners = HashMap<SoftKeyboardToggleListener, SoftKeyboardStateListener>()

    /**
     * Add a new keyboard toggleListener
     * @param activity calling activity
     * @param toggleListener callback
     */
    fun addKeyboardToggleListener(activity: Activity, toggleListener: SoftKeyboardToggleListener) {
        removeKeyboardToggleListener(toggleListener)

        listeners[toggleListener] = SoftKeyboardStateListener(activity, toggleListener)
    }

    /**
     * Remove a registered toggleListener
     * @param toggleListener [SoftKeyboardToggleListener]
     */
    fun removeKeyboardToggleListener(toggleListener: SoftKeyboardToggleListener) {
        val listener = listeners.remove(toggleListener)

        listener?.cleanUp()
    }

    /**
     * Remove all registered keyboard listeners
     */
    fun removeAllKeyboardToggleListeners() {
        listeners.values.forEach { it.cleanUp() }

        listeners.clear()
    }

}
