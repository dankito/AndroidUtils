package net.dankito.utils.android.keyboard

import android.app.Activity
import android.graphics.Rect
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import net.dankito.utils.android.extensions.removeOnGlobalLayoutListener


internal class SoftKeyboardStateListener(activity: Activity, private var callback: SoftKeyboardToggleListener?) : ViewTreeObserver.OnGlobalLayoutListener {

    companion object {
        private const val MagicNumber = 200
    }


    private var rootView: View? = null
    private var screenDensity = 1f

    private var previousValue: Boolean? = null


    init {
        rootView = (activity.findViewById<View>(android.R.id.content) as ViewGroup).getChildAt(0)
        rootView?.viewTreeObserver?.addOnGlobalLayoutListener(this)

        screenDensity = activity.resources.displayMetrics.density
    }


    override fun onGlobalLayout() {
        rootView?.let { rootView ->
            val rect = Rect()
            rootView.getWindowVisibleDisplayFrame(rect)

            val heightDiff = rootView.rootView.height - (rect.bottom - rect.top)
            val dp = heightDiff / screenDensity
            val isVisible = dp > MagicNumber

            if (callback != null && (previousValue == null || isVisible != previousValue)) {
                previousValue = isVisible
                callback?.onToggleSoftKeyboard(isVisible)
            }
        }
    }


    fun cleanUp() {
        removeGlobalLayoutListener()
    }

    private fun removeGlobalLayoutListener() {
        callback = null

        rootView?.removeOnGlobalLayoutListener(this)

        rootView = null
    }

}