package net.dankito.utils.android.keyboard

import android.app.Activity


class KeyboardState {

    var isKeyboardVisible: Boolean = false
        private set

    private val keyboardUtils = KeyboardUtils()

    private val keyboardToggleListener = object : SoftKeyboardToggleListener {

        override fun onToggleSoftKeyboard(isVisible: Boolean) {
            isKeyboardVisible = isVisible
        }

    }


    fun init(activity: Activity) {
        keyboardUtils.addKeyboardToggleListener(activity, keyboardToggleListener)
    }

    /**
     * It's very important to call cleanUp() when done to avoid memory leaks!
     */
    fun cleanUp() {
        keyboardUtils.removeKeyboardToggleListener(keyboardToggleListener)
    }

}