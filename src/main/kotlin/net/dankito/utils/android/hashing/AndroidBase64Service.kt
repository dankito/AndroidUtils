package net.dankito.utils.android.hashing

import android.util.Base64
import java.nio.charset.Charset


class AndroidBase64Service  {

    companion object {
        val DefaultCharset: Charset = Charset.forName("UTF-8")
    }


    fun encode(stringToEncode: String, charset: Charset = DefaultCharset): String {
        return encode(stringToEncode.toByteArray(charset))
    }

    fun encode(dataToEncode: ByteArray): String {
        return Base64.encodeToString(dataToEncode, Base64.NO_WRAP)
    }

    fun decode(stringToDecode: String, charset: Charset = DefaultCharset): String {
        return String(decodeToBytes(stringToDecode), charset)
    }

    fun decodeToBytes(stringToDecode: String): ByteArray {
        return Base64.decode(stringToDecode, Base64.NO_WRAP)
    }

}
