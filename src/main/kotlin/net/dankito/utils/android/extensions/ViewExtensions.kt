package net.dankito.utils.android.extensions

import android.content.Context
import android.content.res.ColorStateList
import android.os.Build
import android.support.v4.view.ViewCompat
import android.view.MotionEvent
import android.view.View
import android.view.ViewTreeObserver
import android.view.inputmethod.InputMethodManager
import org.slf4j.LoggerFactory


fun View.showKeyboard() {
    this.context?.let { context ->
        this.requestFocus()

        val keyboard = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        try {
            keyboard.showSoftInput(this, 0)
        } catch (e: Exception) { // couldn't believe it, inside keyboard.showSoftInput() may a NullPointerException occurs
            LoggerFactory.getLogger("ViewExtensions").error("Call to InputMethodManager.showSoftInput() resulted in an error", e)
        }
    }
}

fun View.showKeyboardDelayed(delayMillis: Long = 50L) {
    this.postDelayed({
        this.showKeyboard()
    }, delayMillis)
}

fun View.hideKeyboard(flags: Int = 0) {
    this.context?.let { context ->
        val keyboard = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        keyboard.hideSoftInputFromWindow(this.windowToken, flags)
    }
}

fun View.hideKeyboardDelayed(delayMillis: Long = 50L, flags: Int = 0) {
    this.postDelayed({
        this.hideKeyboard(flags)
    }, delayMillis)
}


fun View.isVisible(): Boolean {
    return this.visibility == View.VISIBLE
}

fun View.isInvisible(): Boolean {
    return this.visibility == View.INVISIBLE
}

fun View.isGone(): Boolean {
    return this.visibility == View.GONE
}


fun View.getPixelSizeForDisplay(deviceIndependentPixel: Int): Int {
    return (deviceIndependentPixel * getDisplayDensity()).toInt()
}

fun View.getLayoutSize(sizeInDp: Int): Int {
    if(sizeInDp >= 0) {
        return getPixelSizeForDisplay(sizeInDp)
    }
    else { // e.g. ViewGroup.LayoutParams.MATCH_PARENT
        return sizeInDp
    }
}

fun View.getLocationOnScreen(): IntArray {
    val location = IntArray(2)
    this.getLocationOnScreen(location)

    return location
}

fun View.getLocationOnScreenX(): Int {
    return getLocationOnScreen()[0]
}

fun View.getLocationOnScreenY(): Int {
    return getLocationOnScreen()[1]
}

fun View.getLocationInWindow(): IntArray {
    val location = IntArray(2)
    this.getLocationInWindow(location)

    return location
}

fun View.getLocationInWindowX(): Int {
    return getLocationInWindow()[0]
}

fun View.getLocationInWindowY(): Int {
    return getLocationInWindow()[1]
}

fun View.getDisplayDensity(): Float {
    return this.context.resources.displayMetrics.density
}

fun View.getResourceIdentifier(identifierName: String, resourceType: String): Int? {
    return this.context.getResourceIdentifier(identifierName, resourceType)
}


fun View.setPadding(paddingTopBottomLeftRight: Int) {
    this.setPadding(paddingTopBottomLeftRight, paddingTopBottomLeftRight, paddingTopBottomLeftRight, paddingTopBottomLeftRight)
}


fun View.setBackgroundTintColor(color: Int) {
    val colorStateList = ColorStateList.valueOf(color)

    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        this.backgroundTintList = colorStateList
    }
    else {
        ViewCompat.setBackgroundTintList(this, colorStateList)
    }
}


fun View.executeActionAfterMeasuringSize(forceWaitForMeasuring: Boolean = false, action: () -> Unit) {
    if(this.measuredHeight == 0 || forceWaitForMeasuring) { // in this case we have to wait till height is determined -> set OnGlobalLayoutListener
        var layoutListener: ViewTreeObserver.OnGlobalLayoutListener? = null // have to do it that complicated otherwise in OnGlobalLayoutListener we cannot access layoutListener variable
        layoutListener = ViewTreeObserver.OnGlobalLayoutListener {
            removeOnGlobalLayoutListener(layoutListener)

            action()
        }

        this.viewTreeObserver.addOnGlobalLayoutListener(layoutListener)
    }
    else {
        action()
    }
}

fun View.removeOnGlobalLayoutListener(layoutListener: ViewTreeObserver.OnGlobalLayoutListener?) {
    if(Build.VERSION.SDK_INT > Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
        this.viewTreeObserver.removeOnGlobalLayoutListener(layoutListener)
    }
    else {
        @Suppress("OverridingDeprecatedMember")
        this.viewTreeObserver.removeGlobalOnLayoutListener(layoutListener)
    }
}

fun View.isTouchInsideView(event: MotionEvent): Boolean {
    return isPointInsideView(event.rawX, event.rawY)
}

/**
 * Determines if given points are inside view
 * @param x - x coordinate of point
 * *
 * @param y - y coordinate of point
 * *
 * @param view - view object to compare
 * *
 * @return true if the points are within view bounds, false otherwise
 */
fun View.isPointInsideView(x: Float, y: Float): Boolean {
    val location = IntArray(2)
    this.getLocationOnScreen(location)
    val viewX = location[0]
    val viewY = location[1]

    //point is inside view bounds
    if (x > viewX && x < viewX + this.getWidth() &&
            y > viewY && y < viewY + this.getHeight()) {
        return true
    }
    else {
        return false
    }
}