package net.dankito.utils.android.extensions

import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.content.res.ColorStateList
import android.graphics.drawable.Drawable
import android.support.v4.content.ContextCompat


fun Context.getColorFromResource(colorResourceId: Int): Int {
    return ContextCompat.getColor(this, colorResourceId)
}

fun Context.getDimension(dimensionResourceId: Int): Int {
    return resources.getDimension(dimensionResourceId).toInt()
}

fun Context.createColorStateList(tintColorResource: Int): ColorStateList {
    if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
        return resources.getColorStateList(tintColorResource, theme)
    }
    else {
        @Suppress("DEPRECATION")
        return resources.getColorStateList(tintColorResource)
    }
}

fun Context.getResourceIdentifier(identifierName: String, resourceType: String): Int? {
    val identifier = resources.getIdentifier(identifierName, resourceType, (this as Activity).packageName)

    return if (identifier > 0) // returns 0 in case resource is not found
                identifier
            else
                null
}

fun Context.getColorForAttributeId(attributeResourceId: Int): Int? {
    val defaultValue = Int.MAX_VALUE // is the default value valid for all resources or only for colors?

    val retrievedColor = getColorForAttributeId(attributeResourceId, defaultValue)

    return if (retrievedColor != defaultValue) retrievedColor else null
}

fun Context.getColorForAttributeIdWithFallbackColorId(attributeResourceId: Int, fallbackColorResourceId: Int): Int {
    val defaultValue = Int.MAX_VALUE // is the default value valid for all resources or only for colors?

    val retrievedColor = getColorForAttributeId(attributeResourceId, defaultValue)

    return if (retrievedColor != defaultValue) retrievedColor else getColorFromResource(fallbackColorResourceId)
}

fun Context.getColorForAttributeId(attributeResourceId: Int, defaultColor: Int): Int {
    val attributeResourceIdArray = intArrayOf(attributeResourceId)
    val indexOfAttribute = 0

    val typedArray = this.obtainStyledAttributes(attributeResourceIdArray)
    val color = typedArray.getColor(indexOfAttribute, defaultColor)
    typedArray.recycle()

    return color
}

fun Context.getDrawableForAttributeId(attributeResourceId: Int): Drawable {
    val attributeResourceIdArray = intArrayOf(attributeResourceId)
    val indexOfAttribute = 0

    val typedArray = this.obtainStyledAttributes(attributeResourceIdArray)
    val drawable = typedArray.getDrawable(indexOfAttribute)
    typedArray.recycle()

    return drawable
}

fun Context.getResourceIdForAttributeId(attributeResourceId: Int): Int {
    return getResourceIdForAttributeId(attributeResourceId, 0)
}

fun Context.getResourceIdForAttributeId(attributeResourceId: Int, defaultValue: Int): Int {
    val attributeResourceIdArray = intArrayOf(attributeResourceId)
    val indexOfAttribute = 0

    val typedArray = this.obtainStyledAttributes(attributeResourceIdArray)
    val resourceId = typedArray.getResourceId(indexOfAttribute, defaultValue)
    typedArray.recycle()

    return resourceId
}

/**
 * Not always Context is an instance of Activity.
 * E.g. if a theme is set Context is an instance of ContextThemeWrapper.
 * This method tries to get current Activity for Context.
 */
fun Context.asActivity(): Activity? {
    if (this is Activity) {
        return this
    }
    else if (this is ContextWrapper) {
        return this.baseContext.asActivity()
    }

    // should never happen (except if called for a Service, ...)
    return null
}