package net.dankito.utils.android.extensions

import android.app.Activity
import android.util.TypedValue
import net.dankito.utils.android.R
import net.dankito.utils.android.ui.theme.ThemeConstants


/**
 * Returns current theme's name if attribute [net.dankito.utils.android.R.attr.themeName] is set on theme / style resource, null otherwise.
 */
val Activity.themeName: String?
    get() {
        val outValue = TypedValue()
        this.theme.resolveAttribute(R.attr.themeName, outValue, true)

        return outValue.string?.toString()
    }

/**
 * Returns true if attribute [net.dankito.utils.android.R.attr.themeName] is set on theme / style resource and
 * its value equals [net.dankito.utils.android.ui.theme.ThemeConstants.LightThemeName].
 */
val Activity.hasLightTheme: Boolean
    get() {
        return ThemeConstants.LightThemeName == themeName
    }

/**
 * Returns true if attribute [net.dankito.utils.android.R.attr.themeName] is set on theme / style resource and
 * its value equals [net.dankito.utils.android.ui.theme.ThemeConstants.DarkThemeName].
 */
val Activity.hasDarkTheme: Boolean
    get() {
        return ThemeConstants.DarkThemeName == themeName
    }