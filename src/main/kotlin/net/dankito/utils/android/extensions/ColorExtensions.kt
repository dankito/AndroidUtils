package net.dankito.utils.android.extensions

import android.graphics.Color


class ColorExtensions {

    companion object {

        fun setRed(color: Int, red: Int): Int {
            return Color.argb(Color.alpha(color), red, Color.green(color), Color.blue(color))
        }

        fun setGreen(color: Int, green: Int): Int {
            return Color.argb(Color.alpha(color), Color.red(color), green, Color.blue(color))
        }

        fun setBlue(color: Int, blue: Int): Int {
            return Color.argb(Color.alpha(color), Color.red(color), Color.green(color), blue)
        }

        fun setTransparency(color: Int, alpha: Int): Int {
            return Color.argb(alpha, Color.red(color), Color.green(color), Color.blue(color))
        }

    }

}