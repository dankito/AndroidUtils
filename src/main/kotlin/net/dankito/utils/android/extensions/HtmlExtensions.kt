package net.dankito.utils.android.extensions

import android.content.Context
import android.os.Build
import android.text.Html
import android.text.Spanned
import android.text.TextUtils
import net.dankito.utils.android.HtmlLocalResourceImageGetter


open class HtmlExtensions {

    companion object {

        fun getSpannedFromHtml(context: Context, stringResourceId: Int): Spanned {
            val html = context.getText(stringResourceId).toString()

            return html.getSpannedFromHtml()
        }

        fun getPlainTextFromHtml(context: Context, stringResourceId: Int): String {
            val html = context.getText(stringResourceId).toString()

            return html.getPlainTextFromHtml()
        }

    }

}


fun String.getSpannedFromHtml(): Spanned {
    return this.getSpannedFromHtmlWithImages()
}

/**
 * context is only needed if html contains images.
 * tintColorResourceId is only needed if you want to colorize the images in html
 */
fun String.getSpannedFromHtmlWithImages(context: Context? = null, tintColorResourceId: Int? = null): Spanned {
    val imageGetter = if(context != null) HtmlLocalResourceImageGetter(context, tintColorResourceId) else null

    val spanned =
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(this, Html.FROM_HTML_MODE_LEGACY, imageGetter, null)
        }
        else {
            @Suppress("DEPRECATION")
            Html.fromHtml(this, imageGetter, null)
        }

    return spanned.trim() as Spanned // Html.fromHtml() sometimes adds new lines add the end -> trim
}


fun String.getPlainTextFromHtml(): String {
    val spanned = this.getSpannedFromHtml() // or use Jsoup
    val chars = CharArray(spanned.length)
    TextUtils.getChars(spanned, 0, spanned.length, chars, 0)

    return String(chars)
}