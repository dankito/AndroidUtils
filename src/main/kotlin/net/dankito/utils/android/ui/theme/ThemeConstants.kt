package net.dankito.utils.android.ui.theme


class ThemeConstants {

    companion object {

        /**
         * Attribute value of [net.dankito.utils.android.R.attr.themeName] for [net.dankito.utils.android.R.style.AppThemeLightBase].
         */
        const val LightThemeName = "AppThemeLightBase"

        /**
         * Attribute value of [net.dankito.utils.android.R.attr.themeName] for [net.dankito.utils.android.R.style.AppThemeDarkBase].
         */
        const val DarkThemeName = "AppThemeDarkBase"

    }

}