package net.dankito.utils.android.ui.view

import android.view.MotionEvent


interface IHandlesTouch {

    fun handlesTouch(event: MotionEvent): Boolean

}