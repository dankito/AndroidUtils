package net.dankito.utils.android.ui.view


interface IHandlesBackButtonPress {

    fun handlesBackButtonPress(): Boolean

}