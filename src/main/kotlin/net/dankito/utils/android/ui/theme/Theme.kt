package net.dankito.utils.android.ui.theme


class Theme @JvmOverloads constructor(

        /**
         * The internal theme name. Used e.g. for storing in preferences.
         */
        val name: String,

        /**
         * The resource id from R.style.
         */
        val styleResourceId: Int

) {

    override fun toString(): String {
        return name
    }

}