package net.dankito.utils.android

import android.text.Editable
import android.text.TextWatcher


class GenericTextWatcher(private val onTextChanged: ((text: CharSequence, start: Int, before: Int, count: Int) -> Unit)? = null,
                         private val beforeTextChanged: ((text: CharSequence, start: Int, count: Int, after: Int) -> Unit)? = null,
                         private val afterTextChanged: ((editable: Editable) -> Unit)? = null)
    : TextWatcher {


    override fun beforeTextChanged(text: CharSequence, start: Int, count: Int, after: Int) {
        beforeTextChanged?.invoke(text, start, count, after)
    }

    override fun onTextChanged(text: CharSequence, start: Int, before: Int, count: Int) {
        onTextChanged?.invoke(text, start, before, count)
    }

    override fun afterTextChanged(editable: Editable) {
        afterTextChanged?.invoke(editable)
    }

}