package net.dankito.utils.android.cron

import android.content.BroadcastReceiver
import java.util.*


interface ICronService {

    fun startPeriodicalJob(periodicalCheckTimeMillis: Long, classThatReceivesBroadcastWhenPeriodElapsed: Class<out BroadcastReceiver>): Int

    fun startPeriodicalJob(startTime: Calendar, intervalMillis: Long, classThatReceivesBroadcastWhenPeriodElapsed: Class<out BroadcastReceiver>): Int

    fun cancelPeriodicalJob(cronJobTokenNumber: Int): Boolean

}
