package net.dankito.utils.android.cron

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import org.slf4j.LoggerFactory
import java.util.*
import java.util.concurrent.ConcurrentHashMap


open class AlarmManagerCronService : ICronService {

    companion object {

        protected val CRON_JOB_TOKEN_NUMBER_EXTRA_NAME = "CronJobTokenNumber"


        protected var startedJobs: MutableMap<Int, PendingIntent> = ConcurrentHashMap()

        protected var NextCronJobTokenNumber = 1


        private val log = LoggerFactory.getLogger(AlarmManagerCronService::class.java)

    }


    protected lateinit var context: Context


    protected val alarmManager: AlarmManager
        get() = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager


    // needed for Entry in AndroidManifest.xml, in this way AlarmManagerCronService gets waked up by Broadcast
    constructor() {

    }

    constructor(context: Context) {
        this.context = context
    }


    /**
     *
     * @param periodicalCheckTimeMillis
     * @param classThatReceivesBroadcastWhenPeriodElapsed
     * @return The cron job token number to uniquely identify started cron job
     */
    override fun startPeriodicalJob(periodicalCheckTimeMillis: Long, classThatReceivesBroadcastWhenPeriodElapsed: Class<out BroadcastReceiver>): Int {
        val startTime = Calendar.getInstance()
        startTime.timeInMillis = System.currentTimeMillis()
        startTime.add(Calendar.MILLISECOND, periodicalCheckTimeMillis.toInt())

        return startPeriodicalJob(startTime, periodicalCheckTimeMillis, classThatReceivesBroadcastWhenPeriodElapsed)
    }

    /**
     *
     * @param startTime
     * @param classThatReceivesBroadcastWhenPeriodElapsed
     * @return The cron job token number to uniquely identify started cron job
     */
    override fun startPeriodicalJob(startTime: Calendar, intervalMillis: Long, classThatReceivesBroadcastWhenPeriodElapsed: Class<out BroadcastReceiver>): Int {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = System.currentTimeMillis()
        // if this time today has already passed, schedule for next day
        if (Calendar.getInstance().get(Calendar.HOUR_OF_DAY) >= startTime.get(Calendar.HOUR_OF_DAY) && Calendar.getInstance().get(Calendar.MINUTE) >= startTime.get(Calendar.MINUTE)) {
            calendar.add(Calendar.DAY_OF_YEAR, 1)
        }

        calendar.set(Calendar.HOUR_OF_DAY, startTime.get(Calendar.HOUR_OF_DAY))
        calendar.set(Calendar.MINUTE, startTime.get(Calendar.MINUTE))
        calendar.set(Calendar.SECOND, 0)

        val tokenNumber = NextCronJobTokenNumber++

        val intent = Intent(context, classThatReceivesBroadcastWhenPeriodElapsed)
        intent.putExtra(CRON_JOB_TOKEN_NUMBER_EXTRA_NAME, tokenNumber)
        val pendingIntent = PendingIntent.getBroadcast(context, tokenNumber, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        alarmManager.setInexactRepeating(AlarmManager.RTC, calendar.timeInMillis,
                intervalMillis, pendingIntent)

        startedJobs[tokenNumber] = pendingIntent

        log.info("Started a periodical cron job with token number " + tokenNumber + " for " + calendar.time)

        return tokenNumber
    }


    override fun cancelPeriodicalJob(cronJobTokenNumber: Int): Boolean {
        log.info("Trying to cancel cron job with token number $cronJobTokenNumber")
        val pendingIntent = startedJobs.remove(cronJobTokenNumber)

        if (pendingIntent != null) {
            alarmManager.cancel(pendingIntent)
            log.info("Cancelled cron job with token number $cronJobTokenNumber")
            return true
        }

        log.warn("No cron job found for token number $cronJobTokenNumber")
        return false
    }

}
